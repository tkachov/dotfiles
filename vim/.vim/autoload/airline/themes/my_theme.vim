let g:airline#themes#my_theme#palette = {}
let s:gui00 = "#282a2e"
let s:gui01 = "#b71c1c"
let s:gui02 = "#00695c"
let s:gui03 = "#f57c00"
let s:gui04 = "#0288d1"
let s:gui05 = "#6a1b9a"
let s:gui06 = "#1b5e20"
let s:gui07 = "#455a64"
let s:gui08 = "#373b41"
let s:gui09 = "#ef5350"
let s:gui0A = "#26a68a"
let s:gui0B = "#ff1726"
let s:gui0C = "#4fc3f7"
let s:gui0D = "#ba68c8"
let s:gui0E = "#66bb6a"
let s:gui0F = "#b0bec5"
let s:gui10 = "#000000"
let s:guiFF = "#ffffff"

let s:cterm00 = 0
let s:cterm01 = 1
let s:cterm02 = 2
let s:cterm03 = 3
let s:cterm04 = 4
let s:cterm05 = 5
let s:cterm06 = 6
let s:cterm07 = 7
let s:cterm08 = 8
let s:cterm09 = 9
let s:cterm0A = 10
let s:cterm0B = 11
let s:cterm0C = 12
let s:cterm0D = 13
let s:cterm0E = 14
let s:cterm0F = 15
let s:cterm10 = 16
let s:ctermFF = 255

let s:N1   = [ s:gui0F, s:gui00, s:cterm0F, s:cterm00 ]
let s:N2   = [ s:gui0F, s:gui08, s:cterm0F, s:cterm08 ]
let s:N3   = [ s:gui00, s:gui0F, s:cterm00, s:cterm0F ]
let g:airline#themes#my_theme#palette.normal = airline#themes#generate_color_map(s:N1, s:N2, s:N3)

let s:I1   = [ s:guiFF, s:gui04, s:ctermFF, s:cterm04 ]
let s:I2   = [ s:gui00, s:gui0F, s:cterm00, s:cterm0F ]
let s:I3   = [ s:gui0F, s:gui00, s:cterm0F, s:cterm00 ]
let g:airline#themes#my_theme#palette.insert = airline#themes#generate_color_map(s:I1, s:I2, s:I3)

let s:R1   = [ s:guiFF, s:gui01, s:ctermFF, s:cterm01 ]
let s:R2   = [ s:gui00, s:gui0F, s:cterm00, s:cterm0F ]
let s:R3   = [ s:gui0F, s:gui00, s:cterm0F, s:cterm00 ]
let g:airline#themes#my_theme#palette.replace = airline#themes#generate_color_map(s:R1, s:R2, s:R3)

let s:V1   = [ s:guiFF, s:gui03, s:cterm10, s:cterm03 ]
let s:V2   = [ s:gui00, s:gui0F, s:cterm00, s:cterm0F ]
let s:V3   = [ s:gui0F, s:gui00, s:cterm0F, s:cterm00 ]
let g:airline#themes#my_theme#palette.visual = airline#themes#generate_color_map(s:V1, s:V2, s:V3)

let s:IA1   = [ s:gui0F, s:gui00, s:cterm0F, s:cterm00 ]
let s:IA2   = [ s:gui0F, s:gui08, s:cterm0F, s:cterm08 ]
let s:IA3   = [ s:gui0F, s:gui00, s:cterm0F, s:cterm00 ]
let g:airline#themes#my_theme#palette.inactive = airline#themes#generate_color_map(s:IA1, s:IA2, s:IA3)

