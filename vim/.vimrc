" spaces instead of tabs
filetype plugin indent on
set tabstop=4
set shiftwidth=4
set expandtab

" syntax highlighting
syntax on

" line numbers
set number

" enable third party line
" set laststatus=2
" set noshowmode

" Use vim defaults instead of 100% vi-compability
set nocompatible

" More powerfule backspacing
set backspace=indent,eol,start

" Lower priority for certain file types when doing tab completion
set suffixes=.bak,~,.swp,.o,.info,.aux,.log,.dvi,.bbl,.blg,.brf,.cb,.ind,.idx,.ilg,.inx,.out,.toc,.png,.jpg

" Show a few lines around the cursor
set scrolloff=10

" minimize delay when returning to normal mode
set ttimeoutlen=100

set wrap nowrap

" Enable mouse support when available
" if has('mouse')
"     set mouse=a
" endif

" " init plugins
" call plug#begin()
" Plug 'lervag/vimtex'
" " Plug 'vim-airline/vim-airline'
" call plug#end()
" 
" " vimtex settings
" let g:vimtex_compiler_method = 'latexrun'
" let g:vimtex_compiler_latexrun = {'options' : ['--bibtex-cmd biber']}

" airline settings
" let g:airline_extensions = []
" let g:airline_powerline_fonts = 1
" let g:airline_theme='my_theme'
" let g:airline_skip_empty_sections = 1

