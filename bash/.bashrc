#
# ~/.bashrc
#

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

[ -r /usr/share/bash-completion/bash_completion   ] && . /usr/share/bash-completion/bash_completion

# standard prompt
# PS1='[\u@\h \W]\$ '

# two line prompt
PS1="\n\[\e[0;1m\]\u@\[\e[1;34m\]\h\[\e[0;1m\] \w\n❯ \[\e[0m\]"

# minimal prompt
# PS1='\W > '

# With lines
# PS1='┌─[\[\e[1;33m\]\u\[\e[00m\]][\[\e[1;34m\]\w\[\e[00m\]]\n└──[ '

# auto cd
shopt -s autocd

# command not found
#source /usr/share/doc/pkgfile/command-not-found.bash

# enable Ctrl+Shift+t in termite
source /etc/profile.d/vte.sh

# Fix ssh issues with termite
TERM="xterm-256color"

# aliases
alias ls='ls --color=auto'
alias la='ls -a --color=auto'
alias ungz='tar -xzvf'
alias untar='tar -vf'
alias update-mirrors='sudo reflector --verbose -l 300 --sort rate --save /etc/pacman.d/mirrorlist'
alias rmv='mv ~/.trash/'

