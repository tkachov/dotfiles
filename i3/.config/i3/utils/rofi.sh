#!/bin/bash

#rofi -show run -width 50 -lines 8 -font "pango:RobotoMono 18" -eh 1.5 rofi -color-window "#978965, #402e2e, #000000" -color-normal "#978965, #402e2e, #766b4f, #402e2e, #978965"

rofi -show run -font "pango:RobotoMono 16" -fg "#d3d0c8" -bg "#2d2d2d" -hlfg "#2d2d2d" -hlbg "#d3d0c8" -opacity 85 -bc "#FFFFFF" -bw 3
